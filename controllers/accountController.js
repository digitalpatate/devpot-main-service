const mongoose = require('mongoose');
const AccountModel = require('../models/accountModel');
const ProjectModel = require('../models/projectModel');
const { handleError, ErrorHandler } = require('../helpers/error');

/**
 * @swagger
 * /accounts:
 *   post:
 *     tags:
 *     - "Account"
 *     summary: "Create an account"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "user"
 *       in: "formData"
 *       description: "The userId given when logged in"
 *       required: true
 *       type: "string"
 *     - name: "firstname"
 *       in: "formData"
 *       description: "The user firstname"
 *       required: false
 *       type: "string"
 *     - name: "lastname"
 *       in: "formData"
 *       description: "The user lastname"
 *       required: false
 *       type: "string"
 *     - name: "biography"
 *       in: "formData"
 *       description: "The user biography"
 *       required: false
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       200:
 *         description: "Account creataed"
 */
const createAccount = (req, res) => {
  const newAccount = new AccountModel(req.body);
  newAccount.save((saveErr) => {
    if (saveErr) return handleError(new ErrorHandler(400, saveErr.errors), res);
    return res.json({ data: newAccount, error: false, message: 'Successfully saved' });
  });
};

/**
 * @swagger
 * /accounts/{accountId}:
 *   get:
 *     tags:
 *     - "Account"
 *     summary: "Get an account"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "accountId"
 *       in: "path"
 *       description: "The unique id of the account"
 *       required: true
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       404:
 *         description: "Account not found"
 *       200:
 *         description: "Account found"
 */
const getAccountById = (req, res) => {
  AccountModel.findById(req.params.id, (findErr, account) => {
    if (!account) return handleError(new ErrorHandler(404, 'Account not found'), res);
    if (findErr) return handleError(new ErrorHandler(400, findErr.errors), res);
    return res.json({ data: account, error: false });
  });
};

/**
 * @swagger
 * /accounts/{accountId}:
 *   get:
 *     tags:
 *     - "Account"
 *     summary: "Get accounts"
 *     produces:
 *     - "application/json"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       404:
 *         description: "Account not found"
 *       200:
 *         description: "Account found"
 */
const getAccounts = (req, res) => {
  AccountModel.find(req.query).skip(req.offset).limit(req.limit).sort(req.sort_by_direction + req.sort_by_field)
    .exec((findErr, account) => {
      if (findErr) throw new ErrorHandler(400, findErr.message);
      return res.json({ data: account, error: false });
    });
};
const getAccountFavorite = (req, res) => {
  AccountModel.findById(req.params.id).select('favourites').populate('favourites')
    .exec((findErr, account) => {
      if (findErr) throw new ErrorHandler(400, findErr.message);
      return res.json({ data: account, error: false });
    });
};

/**
 * @swagger
 * /accounts/{accountId}:
 *   put:
 *     tags:
 *     - "Account"
 *     summary: "update an account"
 *     security:
 *      - JWT : []
 *     consumes:
 *     - "application/x-www-form-urlencoded"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "firstname"
 *       in: "formData"
 *       description: "The user firstname"
 *       required: false
 *       type: "string"
 *     - name: "lastname"
 *       in: "formData"
 *       description: "The user lastname"
 *       required: false
 *       type: "string"
 *     - name: "biography"
 *       in: "formData"
 *       description: "The user biography"
 *       required: false
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       401:
 *         description: "Invalid authentication"
 *       403:
 *         description: "Access denied "
 *       404:
 *         description: "devlog not found"
 *       200:
 *         description: "Devlog created"
 */
const updateAccount = (req, res) => {
  AccountModel.findById(req.params.id, (findErr, account) => {
    if (!account) return handleError(new ErrorHandler(404, 'Account not found'), res);
    if (findErr) return handleError(new ErrorHandler(400, findErr.errors), res);

    if (account._id.toString() !== req.requester) return handleError(new ErrorHandler(403, 'You are not authorized to access this resource'), res);

    if (req.body.user) return handleError(new ErrorHandler(400, 'You cannot change the userId reference'), res);

    Object.assign(account, req.body);
    account.save((saveErr) => {
      if (saveErr) return handleError(new ErrorHandler(400, saveErr.errors), res);
      return res.json({ data: account, error: false, message: 'Successfully saved' });
    });
  });
};
/**
 * @swagger
 * /accounts/{accountId}:
 *   delete:
 *     tags:
 *     - "Account"
 *     summary: "delete an account"
 *     security:
 *      - JWT : []
 *     consumes:
 *     - "application/x-www-form-urlencoded"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "accountId"
 *       in: "path"
 *       description: "The unique id of the account"
 *       required: true
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       401:
 *         description: "Invalid authentication"
 *       403:
 *         description: "Access denied "
 *       404:
 *         description: "Account not found"
 *       200:
 *         description: "Account deleted"
 */
const deleteAccount = (req, res) => {
  AccountModel.findById(req.params.id, (findErr, account) => {
    if (!account) return handleError(new ErrorHandler(404, 'Account not found'), res);
    if (findErr) return handleError(new ErrorHandler(400, findErr.errors), res);
    if (account._id.toString() !== req.requester) return handleError(new ErrorHandler(403, 'You are not authorized to access this resource'), res);
    AccountModel.deleteOne({ _id: req.params.id }, (deleteError) => {
      if (deleteError) return handleError(new ErrorHandler(400, deleteError.errors), res);
      return res.json({ data: null, error: false, message: 'Successfully deleted' });
    });
  });
};

/**
 * @swagger
 * /accounts/{accountId}/favourites/:projectId:
 *   post:
 *     tags:
 *     - "Account"
 *     summary: "Add a project to favourites"
 *     security:
 *      - JWT : []
 *     consumes:
 *     - "application/x-www-form-urlencoded"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "accountId"
 *       in: "path"
 *       description: "The unique id of the account"
 *       required: true
 *       type: "string"
 *     - name: "projectId"
 *       in: "path"
 *       description: "The unique id of the project"
 *       required: true
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       401:
 *         description: "Invalid authentication"
 *       403:
 *         description: "Access denied "
 *       404:
 *         description: "Account or project not found"
 *       200:
 *         description: "Project added to favourites"
 */
const toggleFavourite = (req, res) => {
  AccountModel.findById(req.params.id, (findErr, account) => {
    if (!account) return handleError(new ErrorHandler(404, 'Account not found'), res);
    if (findErr) return handleError(new ErrorHandler(400, findErr.errors), res);
    if (account._id.toString() !== req.requester) return handleError(new ErrorHandler(403, 'You are not authorized to access this resource'), res);
    ProjectModel.findById(req.params.projectId, (findProjectErr, project) => {
      if (!project) return handleError(new ErrorHandler(404, 'project not found'), res);
      if (findProjectErr) return handleError(new ErrorHandler(400, findErr.errors), res);
      if (account.favourites.indexOf(req.params.projectId) === -1) {
        account.favourites.push(mongoose.Types.ObjectId(req.params.projectId));
        project.favouritesCount += 1;
      } else {
        account.favourites = account.favourites.filter(projectId => projectId.toString() !== req.params.projectId);
        project.favouritesCount -= 1;
      }
      project.save((projectSaveErr) => {
        if (projectSaveErr) return handleError(new ErrorHandler(400, projectSaveErr.errors), res);
        account.save((accountSaveErr) => {
          if (accountSaveErr) return handleError(new ErrorHandler(400, accountSaveErr.errors), res);
          return res.json({ data: account, error: false, message: 'Project added to favourite' });
        });
      });
    });
  });
};

module.exports = {
  createAccount,
  getAccountById,
  updateAccount,
  deleteAccount,
  toggleFavourite,
  getAccounts,
  getAccountFavorite,
};
