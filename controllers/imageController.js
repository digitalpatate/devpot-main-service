const { ErrorHandler } = require('../helpers/error');
const { getPath } = require('../helpers/image');

/**
 * @swagger
 * /:type/:id/:imageName:
 *   get:
 *     tags:
 *     - "Image"
 *     summary: "Get a specific image"
 *     produces:
 *     - "image/png"
 *     parameters:
 *     - name: "type"
 *       in: "path"
 *       description: "Can take for value devlogs or projects"
 *       required: true
 *       type: "string"
 *     - name: "id"
 *       in: "path"
 *       description: "The unique id of a devlog or project"
 *       required: true
 *       type: "string"
 *     - name: "imageName"
 *       in: "path"
 *       description: "The name of the image"
 *       required: true
 *       type: "string"
 *     responses:
 *       200:
 *         description: "Devlogs found"
 */
const getImage = (req, res) => {
  const type = req.url.split('/')[1];
  res.sendFile(`${process.env.UPLOAD_PATH}/${type}/${req.params.id}/${req.params.imagename}`, { root: '.' });
};

/**
 * @swagger
 * /:type/:id:
 *   post:
 *     tags:
 *     - "Image"
 *     summary: "Upload a specific image"
 *     produces:
 *     - "application/json"
 *     consumes:
 *      - "multipart/form-data"
 *     parameters:
 *     - name: "type"
 *       in: "path"
 *       description: "Can take for value devlogs or projects"
 *       required: true
 *       type: "string"
 *     - name: "id"
 *       in: "path"
 *       description: "The unique id of a devlog or project"
 *       required: true
 *       type: "string"
 *     - name: "file"
 *       in: "formData"
 *       description: "file to upload"
 *       required: true
 *       type: "file"
 *     responses:
 *       200:
 *         description: "Devlogs found"
 */
const createImage = (req, res) => {
  const { file } = req;
  console.log(file);
  if (!file) {
    throw new ErrorHandler(400, 'You have to give an image in file field');
  }
  return res.json({ data: { path: getPath(req) }, error: false, message: 'Image successfully uploaded' });
};


module.exports = {
  getImage,
  createImage,
};
