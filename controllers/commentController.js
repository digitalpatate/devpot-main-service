const CommentModel = require('../models/commentModel');
const ProjectModel = require('../models/projectModel');
const DevlogModel = require('../models/devlogModel');

const { handleError, ErrorHandler } = require('../helpers/error');

const deleteResponses = (commentId) => {
  CommentModel.findById(commentId,(findErr, comment) => {
    if (!comment) return;
    comment.responses.forEach((responseId) => {
      deleteResponses(responseId);
      comment.remove().catch(err => console.log(err));
    });
  });
};

const createComment = async (req, res) => {
  if (!['Project', 'Devlog', 'Comment'].includes(req.body.parentType)) return handleError(new ErrorHandler(400, 'The parentType must be either a devlog. comment or a project'), res);
  Object.assign(req.body, { author: req.requester });
  let query;
  if (req.body.parentType === 'Project') {
    query = ProjectModel.findById(req.body.parent);
  } else if (req.body.parentType === 'Comment') {
    query = CommentModel.findById(req.body.parent);
  } else if (req.body.parentType === 'Devlog') {
    query = DevlogModel.findById(req.body.parent);
  }

  const result = await query.exec();
  if (!result) return handleError(new ErrorHandler(404, `${req.body.parentType} not found`), res);
  const newComment = new CommentModel(req.body);


  if (req.body.parentType === 'Comment') {
    result.responses.push(newComment._id);
    await result.save((err2) => {
      if (err2) return handleError(new ErrorHandler(400, err2.errors), res);
    });
  }
  newComment.save((err) => {
    if (err) return handleError(new ErrorHandler(400, err.errors), res);
    return res.json({ data: newComment, error: false, message: 'Successfully saved' });
  });
};

const getAll = (req, res) => {
  CommentModel.find(req.query).skip(req.offset).limit(req.limit)
    .sort(req.sort_by_direction + req.sort_by_field)
    .populate(req.populate)
    .exec((findErr, comments) => {
      if (findErr) throw new ErrorHandler(400, findErr.message);
      return res.json({ data: comments, error: false });
    });
};
const getById = (req, res) => {
  CommentModel.findById(req.params.id, (findErr, comment) => {
    if (!comment) return handleError(new ErrorHandler(404, 'Comment not found'), res);
    if (findErr) throw new ErrorHandler(400, findErr.message);
    return res.json({ data: comment, error: false });
  });
};
const getResponses = (req, res) => {
  CommentModel.findById(req.params.id, (findErr, comment) => {
    if (!comment) return handleError(new ErrorHandler(404, 'Comment not found'), res);
    if (findErr) throw new ErrorHandler(400, findErr.message);
    return res.json({ data: comment.responses, error: false });
  }).populate('responses');
};
const updateComment = (req, res) => {
  CommentModel.findById(req.params.id, (findErr, comment) => {
    if (!comment) return handleError(new ErrorHandler(404, 'Not found'), res);
    if (findErr) return handleError(new ErrorHandler(400, findErr.message), res);
    if (comment.author.toString() !== req.requester) return handleError(new ErrorHandler(403, 'You are not authorized to access this resource'), res);
    if (req.body.parent) return handleError(new ErrorHandler(400, 'You cannot change the parent reference'), res);
    if (req.body.parentType) return handleError(new ErrorHandler(400, 'You cannot change the type of the parent'), res);
    if (req.body.author) return handleError(new ErrorHandler(400, 'You cannot change the author'), res);
    Object.assign(comment, req.body);
    comment.save((saveErr) => {
      if (saveErr) return handleError(new ErrorHandler(400, saveErr.errors), res);
      return res.json({ data: comment, error: false, message: 'Successfully saved' });
    });
  });
};
const deleteComment = (req, res) => {
  CommentModel.findById(req.params.id, (findErr, comment) => {
    if (!comment) return handleError(new ErrorHandler(404, 'Comment not found'), res);
    if (findErr) return handleError(new ErrorHandler(400, findErr.message), res);
    if (comment.author.toString() !== req.requester) return handleError(new ErrorHandler(403, 'You are not authorized to access this resource'), res);
    deleteResponses(comment._id);
    CommentModel.deleteOne({ _id: req.params.id }, (deleteError) => {
      if (deleteError) return handleError(new ErrorHandler(400, deleteError.errors), res);
      return res.json({ data: null, error: false, message: 'Successfully deleted' });
    });
  });
};


module.exports = {
  createComment,
  getAll,
  getById,
  getResponses,
  updateComment,
  deleteComment,
};
