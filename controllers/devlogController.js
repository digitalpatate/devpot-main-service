
const Devlog = require('../models/devlogModel');
const Project = require('../models/projectModel');
const { handleError, ErrorHandler } = require('../helpers/error');


const getDevlog = (req, res) => {
  req.query.published = req.query.published || true;
  Devlog.find(req.query).skip(req.offset).limit(req.limit)
    .sort(req.sort_by_direction + req.sort_by_field)
    .populate(req.populate)
    .exec((findErr, devlogs) => {
      if (findErr) throw new ErrorHandler(400, findErr.message);
      return res.json({ data: devlogs, error: false });
    });
};

/**
 * @swagger
 * /projects/{projectId}/devlogs:
 *   get:
 *     tags:
 *     - "Devlog"
 *     summary: "Get all devlogs for a given project"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "projectId"
 *       in: "path"
 *       description: "The unique id of the project"
 *       required: true
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       404:
 *         description: "Project not found"
 *       200:
 *         description: "Devlogs found"
 */
const getProjectDevlog = (req, res) => {
  Devlog.findByProjectId(req.params.id, (findErr, devlogs) => {
    if (findErr) throw new ErrorHandler(400, findErr.message);
    res.json({ data: devlogs, error: false });
  });
};

/**
 * @swagger
 * /devlogs/{devlogId}:
 *   get:
 *     tags:
 *     - "Devlog"
 *     summary: "Get a devlog by his id"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "devlogId"
 *       in: "path"
 *       description: "The unique id of the project"
 *       required: true
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       404:
 *         description: "devlog not found"
 *       200:
 *         description: "Devlog found"
 */

const getById = (req, res) => {
  Devlog.findById(req.params.id, (findErr, devlog) => {
    if (findErr) throw new ErrorHandler(400, findErr.message);
    return res.json({ data: devlog, error: false });
  });
};
/**
 * @swagger
 * /devlogs:
 *   post:
 *     tags:
 *     - "Devlog"
 *     summary: "Create a devlog"
 *     security:
 *       - JWT : []
 *     consumes:
 *     - "application/x-www-form-urlencoded"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "title"
 *       in: "formData"
 *       description: "An attractive title for your devlog"
 *       required: true
 *       type: "string"
 *     - name: "content"
 *       in: "formData"
 *       description: "A markdown based content"
 *       required: true
 *       type: "string"
 *     - name: "project"
 *       in: "formData"
 *       description: "the project ref id"
 *       required: true
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       401:
 *         description: "Invalid authentication"
 *       200:
 *         description: "Devlog created"
 */
const createDevlog = (req, res) => {
  if (!req.body.project) throw new ErrorHandler(400, 'You should provide the project id');
  Project.findById(req.body.project, (findErr, project) => {
    if (!project) return handleError(new ErrorHandler(404, 'Project not found'), res);
    if (findErr) return handleError(new ErrorHandler(400, findErr.message), res);
    if (project.owner.toString() !== req.requester) return handleError(new ErrorHandler(403, 'You are not authorized to access this resource'), res);
    const newDevlog = new Devlog(req.body);
    newDevlog.save((err) => {
      if (err) return handleError(new ErrorHandler(400, err.errors), res);
      return res.json({ data: newDevlog, error: false, message: 'Successfully saved' });
    });
  });
};
/**
 * @swagger
 * /devlogs/{devlogId}:
 *   put:
 *     tags:
 *     - "Devlog"
 *     summary: "update a devlog"
 *     security:
 *      - JWT : []
 *     consumes:
 *     - "application/x-www-form-urlencoded"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "devlogId"
 *       in: "path"
 *       description: "The unique id of the project"
 *       required: true
 *       type: "string"
 *     - name: "title"
 *       in: "formData"
 *       description: "An attractive title for your devlog"
 *       required: false
 *       type: "string"
 *     - name: "content"
 *       in: "formData"
 *       description: "A markdown based content"
 *       required: false
 *       type: "string"
 *     - name: "project"
 *       in: "formData"
 *       description: "the project ref id"
 *       required: false
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       401:
 *         description: "Invalid authentication"
 *       403:
 *         description: "Access denied "
 *       404:
 *         description: "devlog not found"
 *       200:
 *         description: "Devlog created"
 */
const updateDevlog = (req, res) => {
  Devlog.findById(req.params.id).populate('project').exec((findErr, devlog) => {
    if (!devlog) return handleError(new ErrorHandler(404, 'Not found'), res);
    if (findErr) return handleError(new ErrorHandler(400, findErr.message), res);
    if (devlog.project.owner.toString() !== req.requester) return handleError(new ErrorHandler(403, 'You are not authorized to access this resource'), res);
    if (req.body.project) return handleError(new ErrorHandler(400, 'You cannot change the project reference'), res);
    Object.assign(devlog, req.body);
    devlog.save((saveErr) => {
      if (saveErr) return handleError(new ErrorHandler(400, saveErr.errors), res);
      return res.json({ data: devlog, error: false, message: 'Successfully saved' });
    });
  });
};

/**
 * @swagger
 * /devlogs/{devlogId}:
 *   delete:
 *     tags:
 *     - "Devlog"
 *     summary: "delete a devlog"
 *     security:
 *      - JWT : []
 *     consumes:
 *     - "application/x-www-form-urlencoded"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - name: "devlogId"
 *       in: "path"
 *       description: "The unique id of the project"
 *       required: true
 *       type: "string"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       401:
 *         description: "Invalid authentication"
 *       403:
 *         description: "Access denied "
 *       404:
 *         description: "devlog not found"
 *       200:
 *         description: "Devlog deleted"
 */
const deleteDevlog = (req, res) => {
  Devlog.findById(req.params.id).populate('project').exec((findErr, devlog) => {
    if (!devlog) return handleError(new ErrorHandler(404, 'Not found'), res);
    if (findErr) return handleError(new ErrorHandler(400, findErr.message), res);
    if (devlog.project.owner.toString() !== req.requester) return handleError(new ErrorHandler(403, 'You are not authorized to access this resource'), res);
    devlog.remove((removeErr, removedDevlog) => {
      if (removeErr) return handleError(new ErrorHandler(400, removeErr.message), res);
      return res.json({ data: removedDevlog, error: false, message: 'Successfully deleted' });
    });
  });
};

const searchDevlog = (req, res) => {
  Devlog.find({ $text: { $search: req.body.data } }).skip(req.offset).limit(req.limit)
    .sort(req.sort_by_direction + req.sort_by_field)
    .populate(req.populate)
    .exec((findErr, devlogs) => {
      if (findErr) throw new ErrorHandler(400, findErr.message);
      return res.json({ data: devlogs, error: false });
    });
};

module.exports = {
  getDevlog,
  createDevlog,
  getProjectDevlog,
  getById,
  updateDevlog,
  deleteDevlog,
  searchDevlog,
};
