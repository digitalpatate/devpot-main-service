const mongoose = require('mongoose');

const { Schema } = mongoose;
const { ObjectId } = Schema;

const { ErrorHandler } = require('../helpers/error');

const commentSchema = new Schema({
  creationDate: {
    type: Date,
    default: Date.now,
  },
  author: {
    type: ObjectId,
    ref: 'Account',
    required: 'A comment needs an author',
  },
  content: {
    type: String,
    required: 'You should provide a description for your project',
  },
  parent: {
    type: Schema.Types.ObjectId,
    required: true,
    refPath: 'parentType',
  },
  parentType: {
    type: String,
    required: true,
    enum: ['Project', 'Comment', 'Devlog'],
  },
  responses: [{
    type: Schema.Types.Mixed,
    ref: 'Comment',
  }],
});

commentSchema.statics.findById = function (id, cb) {
  let commentId;
  try {
    commentId = mongoose.Types.ObjectId(id);
  } catch (e) {
    throw new ErrorHandler(404, 'Comment not found');
  }
  return this.model('Comment').findOne({ _id: commentId }, cb);
};
module.exports = mongoose.model('Comment', commentSchema);
