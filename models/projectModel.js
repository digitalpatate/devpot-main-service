const mongoose = require('mongoose');
const slug = require('mongoose-slug-generator');
const fs = require('fs-extra');


const { ErrorHandler } = require('../helpers/error');

mongoose.plugin(slug);

const { Schema } = mongoose;
const { ObjectId } = Schema;
const projectSchema = new Schema({
  title: {
    type: String,
    required: 'The title is required',
    unique: true,
    text: true,
    index: true,
  },
  slug: {
    type: String,
    slug: 'title',
  },
  description: {
    type: String,
    required: 'You should provide a description for your project',
  },
  imagePath: {
    type: String,
    default: '',
  },
  favouritesCount: {
    type: Number,
    default: 0,
  },
  creationDate: {
    type: Date,
    default: Date.now,
  },
  owner: {
    type: ObjectId,
    ref: 'Account',
    required: 'A project needs an owner account',
  },
});

projectSchema.statics.findById = function (id, cb) {
  let projectId;
  try {
    projectId = mongoose.Types.ObjectId(id);
  } catch (e) {
    throw new ErrorHandler(404, 'Project not found');
  }
  return this.model('Project').findOne({ _id: projectId }, cb);
};
projectSchema.post('save', (project, next) => {
  const path = `${process.env.UPLOAD_PATH}/project/${project._id}`;
  if (!fs.existsSync(path)) {
    fs.mkdirSync(path, { recursive: true }, (err) => {
      if (err) throw new ErrorHandler(500, 'Error in mkdir');
    });
  }
  next();
});
projectSchema.post('remove', (project) => {
  const path = `${process.env.UPLOAD_PATH}/project/${project._id}`;
  if (!fs.existsSync(path)) {
    fs.removeSync(path);
  }
});
module.exports = mongoose.model('Project', projectSchema);
