const mongoose = require('mongoose');
const fs = require('fs-extra');

const immutablePlugin = require('mongoose-immutable');
const slugPlugin = require('mongoose-slug-generator');


const { ErrorHandler } = require('../helpers/error');

mongoose.plugin(slugPlugin);
const { Schema } = mongoose;

const { ObjectId } = Schema;
const devlogSchema = new Schema({
  title: {
    type: String,
    required: 'The title is required',
    index: true,
    unique: true,
    text: true,
  },
  published: {
    type: Boolean,
    required: true,
    default: false,
  },
  slug: {
    type: String,
    slug: 'title',
  },
  content: {
    type: String,
    required: 'You should provide a content for your project',
    text: true,
    index: true,
  },
  creationDate: {
    type: Date,
    default: Date.now,
  },
  project: {
    type: ObjectId,
    ref: 'Project',
    required: 'A devlog belongs to a project',
    immutable: true,
  },
});

devlogSchema.plugin(immutablePlugin);


devlogSchema.statics.findByProjectId = function (id, cb) {
  let projectId;
  try {
    projectId = mongoose.Types.ObjectId(id);
  } catch (e) {
    throw new ErrorHandler(404, 'Project not found');
  }
  return this.model('Devlog').find({ project: projectId }, cb);
};
devlogSchema.statics.findById = function (id, cb) {
  let devlogId;
  try {
    devlogId = mongoose.Types.ObjectId(id);
  } catch (e) {
    throw new ErrorHandler(404, 'Devlog not found');
  }
  return this.model('Devlog').findOne({ _id: devlogId }, cb);
};
devlogSchema.post('save', (devlog) => {
  const path = `${process.env.UPLOAD_PATH}/devlog/${devlog._id}`;
  if (!fs.existsSync(path)) {
    fs.mkdirSync(path, { recursive: true }, (err) => {
      if (err) throw new ErrorHandler(500, 'Error in mkdir');
    });
  }
});
devlogSchema.post('remove', (devlog) => {
  const path = `${process.env.UPLOAD_PATH}/devlog/${devlog._id}`;
  if (!fs.existsSync(path)) {
    fs.removeSync(path);
  }
});

module.exports = mongoose.model('Devlog', devlogSchema);
