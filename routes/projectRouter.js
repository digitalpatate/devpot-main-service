const router = require('express').Router();
const multer = require('multer');
const fs = require('fs-extra');


const projectController = require('../controllers/projectController.js');
const devlogController = require('../controllers/devlogController.js');
const { checkToken } = require('../middleware/auth-token');
const { parsePagination, parseFilter, parsePopulate } = require('../middleware/pagination');

const storage = multer.diskStorage({
  destination(req, file, cb) {
    const path = `${process.env.UPLOAD_PATH}/tmp`;
    fs.mkdirSync(path, { recursive: true });
    cb(null, path);
  },
  filename(req, file, cb) {
    const extension = file.originalname.split('.')[1];
    const basename = file.originalname.split('.')[0];
    cb(null, `${basename}-${Date.now()}.${extension}`);
  },
});
const upload = multer({ storage });

// All projects
router.get('/', parsePagination, parseFilter, parsePopulate, projectController.getProject);
router.post('/search', parsePagination, parseFilter, parsePopulate, projectController.searchProject);

router.post('/', checkToken, upload.single('file'), projectController.createProject);

router.get('/:id', projectController.getById);
router.delete('/:id', checkToken, projectController.deleteProject);
router.put('/:id', checkToken, upload.single('file'), projectController.updateProject);
router.get('/:id/devlogs', devlogController.getProjectDevlog);

module.exports = router;
