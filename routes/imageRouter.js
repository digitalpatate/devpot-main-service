const multer = require('multer');
const router = require('express').Router();
const { storage } = require('../helpers/image');

const controller = require('../controllers/imageController');

const upload = multer({ storage });

router.get('/devlog/:id/:imagename', controller.getImage);
router.get('/project/:id/:imagename', controller.getImage);

router.post('/devlog/:id', upload.single('file'), controller.createImage);
router.post('/project/:id', upload.single('file'), controller.createImage);

module.exports = router;
