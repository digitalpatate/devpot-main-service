const router = require('express').Router();
const controller = require('../controllers/commentController');
const { checkToken } = require('../middleware/auth-token');
const { parsePagination, parseFilter, parsePopulate } = require('../middleware/pagination');


router.post('/', checkToken, controller.createComment);

router.get('/', parsePagination, parseFilter, parsePopulate, controller.getAll);
router.get('/:id', controller.getById);

router.get('/:id/responses', controller.getResponses);

router.put('/:id', checkToken, controller.updateComment);
router.delete('/:id', checkToken, controller.deleteComment);

module.exports = router;
