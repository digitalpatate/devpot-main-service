const router = require('express').Router();
const controller = require('../controllers/accountController');
const { checkToken, checkPresence } = require('../middleware/auth-token');
const { parsePagination, parseFilter } = require('../middleware/pagination');



router.post('/', checkPresence, controller.createAccount);
router.get('/:id', controller.getAccountById);
router.get('/', parsePagination, parseFilter, controller.getAccounts);
router.put('/:id', checkToken, controller.updateAccount);
router.delete('/:id', checkToken, controller.deleteAccount);

router.post('/:id/favourites/:projectId', checkToken, controller.toggleFavourite);
router.get('/:id/favourites', checkToken, controller.getAccountFavorite);


module.exports = router;
