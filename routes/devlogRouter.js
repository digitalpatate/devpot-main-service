const router = require('express').Router();
const controller = require('../controllers/devlogController');
const { checkToken } = require('../middleware/auth-token');
const { parsePagination, parseFilter, parsePopulate } = require('../middleware/pagination');



router.get('/', parsePagination, parseFilter, parsePopulate, controller.getDevlog);
router.post('/search', parsePagination, parseFilter, parsePopulate, controller.searchDevlog);

router.get('/:id', controller.getById);
router.put('/:id', checkToken, controller.updateDevlog);
router.delete('/:id', checkToken, controller.deleteDevlog);
router.post('/', checkToken, controller.createDevlog);

module.exports = router;
