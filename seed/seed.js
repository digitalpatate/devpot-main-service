require('dotenv').config();

const Chance = require('chance');
const axios = require('axios').default;
const slugify = require('slugify');
const jwt = require('jsonwebtoken');
const fs = require('fs-extra');
const path = require('path');
const FormData = require('form-data');

const chance = new Chance();

const API_ERROR = {
  status: 400,
  data: {
    error: true,
    data: null,
    message: 'API Service Error',
  },
};


const ApiService = {
  token: '',
  init() {
    axios.defaults.baseURL = 'http://api.devpot.jackeri.ch';
    axios.defaults.headers = {
      'Access-Control-Allow-Origin': true,
    };
  },
  get(path, withAuth = false) {
    return axios.get(path, {
      headers: withAuth ? this.authorizationHeader() : {},
    }).catch(this.handleError);
  },

  post(path, data, withAuth = false, additionalHeaders = {}) {
    return axios.post(path, data, {
      headers: {
        ...(withAuth ? this.authorizationHeader() : {}),
        ...additionalHeaders,
      },
    }).catch(this.handleError);
  },
  handleError(error) {
    if (error.response) {
      return {
        status: error.response.status,
        data: error.response.data,
      };
    }
    return API_ERROR;
  },

  authorizationHeader() {
    return this.token ? { Authorization: `Bearer ${this.token}` } : {};
  },
};

const nbUserToCreate = process.argv[2]; // 12
const nbMaxProjectPerAccount = process.argv[3]; // 5-6
const nbMaxDevlogPerProject = process.argv[4]; // 1-20

const createUser = async (user) => {
  const { error, data } = await ApiService.post('/auth/register', user, false);
  const response = (!error) ? data.data : null;
  return response;
};
const getAuthToken = async (credential) => {
  const { data } = await ApiService.post('/auth/login', credential, false);
  return data.token;
};
const createAcccount = async (account) => {
  const { error, data } = await ApiService.post('/accounts', account, true);
  const response = (!error) ? data.data : null;
  return response;
};

const createProject = async (project) => {
  const { error, data } = await ApiService.post('/projects', project, true, project.getHeaders());
  const response = (!error) ? data.data : null;
  return response;
};
const createDevlog = async (devlog) => {
  const { error, data } = await ApiService.post('/devlogs', devlog, true);
  const response = (!error) ? data.data : null;
  return response;
};
const createComment = async (comment) => {
  const { error, data } = await ApiService.post('/comments', comment, true);
  const response = (!error) ? data.data : null;
  return response;
};

const getAccounts = async () => {
  const { error, data } = await ApiService.get('/accounts');
  const response = (!error) ? data.data : null;
  return response;
};

const getProjects = async () => {
  const { error, data } = await ApiService.get('/projects');
  const response = (!error) ? data.data : null;
  return response;
};

const getDevlogs = async () => {
  const { error, data } = await ApiService.get('/devlogs');
  const response = (!error) ? data.data : null;
  return response;
};

const getComments = async () => {
  const { error, data } = await ApiService.get('/comments');
  const response = (!error) ? data.data : null;
  return response;
};
const getUser = async (userId) => {
  const { error, data } = await ApiService.get(`/auth/users/${userId}`, true);
  const response = (!error) ? data.data : null;
  return response;
};
const addProjectToFavorite = async (accountId, projectsId) => {
  const { error, data } = await ApiService.post(`/accounts/${accountId}/favourites/${projectsId}`, null, true);
  const response = (!error) ? data.data : null;
  return response;
};
const getRandomArbitrary = (min, max) => Math.random() * (max - min) + min;
const downloadImage = async (url) => {
  const target = path.resolve(__dirname, './test.jpg');
  const writer = fs.createWriteStream(target);

  const response = await axios({
    url,
    method: 'GET',
    responseType: 'stream',
  });

  response.data.pipe(writer);

  return new Promise((resolve, reject) => {
    writer.on('finish', resolve);
    writer.on('error', reject);
  });
};
const generateContentDevlog = async () => {
  let content = '';
  for (let i = 0; i < getRandomArbitrary(1, 3); i += 1) {
    content += `<h2>${chance.animal()} ${chance.word()}</h2>`;
    content += `<img src="https://picsum.photos/seed/${chance.word()}/1200/600">`;
    for (let j = 0; j < getRandomArbitrary(1, 3); j += 1) {
      content += (getRandomArbitrary(1, 6) % 2) ? `<h3>${chance.word()} ${chance.animal()}</h3>` : '';
      content += `<p>${chance.paragraph()}</p>`;
    }
  }
  return content;
};
const generateParagraphs = (min, max) => {
  let str = '';
  for (let i = 0; i < getRandomArbitrary(min, max); i += 1) {
    str += chance.paragraph({ sentences: 4 });
    str += '\n\n\n';
  }
  return str;
};
const generatDdevlogs = async (projectId) => {
  for (let i = 0; i < getRandomArbitrary(5, nbMaxDevlogPerProject); i += 1) {
    const devlog = await createDevlog({
      title: `${chance.animal()} ${chance.word()} ${chance.profession()} ${chance.hashtag()}`,
      content: await generateContentDevlog(),
      project: projectId,
      published: true,
    });
    if (devlog) console.log(`Devlog created: ${devlog.title}`);
  }
};
const generateProjects = async () => {
  for (let i = 0; i < getRandomArbitrary(2, nbMaxProjectPerAccount); i += 1) {
    await downloadImage('https://picsum.photos/1200/600');
    console.log('Image downloaded');
    const formData = new FormData();
    formData.append('title', `${chance.company()} ${chance.word()}`);
    formData.append('description', generateParagraphs(2, 5));
    formData.append('file', fs.createReadStream(path.resolve(__dirname, './test.jpg')));
    const project = await createProject(formData).catch(err => console.log(err));
    if (project) {
      console.log(`Project created: ${project.title}`);
      await generatDdevlogs(project._id);
    }
  }
};

const validateUser = async (userId) => {
  const validationToken = jwt.sign({ data: { userId: userId._id, nextStatus: 'validated' } }, process.env.JWT_SECRET, { expiresIn: '1d' });
  const { error, data } = await ApiService.post(`auth/users/${userId}/tokens/validate`, { token: validationToken }, false);
  const response = (!error) ? data.data : null;
  return response;
};


const genrateBaseData = async () => {
  for (let i = 0; i < nbUserToCreate; i += 1) {
    const name = chance.name();
    const user = await createUser({ username: slugify(name), email: `${slugify(name)}@devpot.ch`, password: '1234' });
    if (user) {
      await validateUser(user._id);
      const token = await getAuthToken({ username: user.username, password: '1234' });
      ApiService.token = token;
      const account = await createAcccount({
        user: user._id, firstname: name.split(' ')[0], lastname: name.split(' ')[1], biography: chance.paragraph(),
      });
      console.log(`Account created: ${account._id}`);
      await generateProjects(account._id);
    }
  }
};
const genrateLikes = async () => {
  const accounts = await getAccounts();
  for (const account of accounts) {
    const projects = await getProjects();
    for (const project of projects) {
      if (Math.random() <0.7) {
        ApiService.token = await jwt.sign({ _id: account.user }, process.env.JWT_SECRET, { expiresIn: '1d', issuer: 'localhost' });
        console.log(`Project ${project.title} add to ${account.firstname} ${account.lastname}'s account`);
        await addProjectToFavorite(account._id, project._id);
      }
    }
  }
};
const generateComments = async () => {
  const accounts = await getAccounts();
  for (const account of accounts) {
    const projects = await getProjects();
    for (const project of projects) {
      if (Math.random() < 0.3) {
        ApiService.token = await jwt.sign({ _id: account.user }, process.env.JWT_SECRET, { expiresIn: '1d', issuer: 'localhost' });
        await createComment({
          content: chance.paragraph(),
          parent: project._id,
          parentType: 'Project',
        });
        console.log(`${account.firstname} ${account.lastname} commented ${project.title}`);
      }
    }
    const devlogs = await getDevlogs();
    for (const devlog of devlogs) {
      if (Math.random() < 0.3) {
        ApiService.token = await jwt.sign({ _id: account.user }, process.env.JWT_SECRET, { expiresIn: '1d', issuer: 'localhost' });
        await createComment({
          content: chance.paragraph(),
          parent: devlog._id,
          parentType: 'Devlog',
        });
        console.log(`${account.firstname} ${account.lastname} commented ${devlog.title}`);
      }
    }
    let comments = await getComments();
    comments = comments.filter(comment => (comment.parentType === 'Project' || comment.parentType === 'Devlog') && comment.author !== account._id);
    for (const comment of comments) {
      if (Math.random() < 0.2) {
        ApiService.token = await jwt.sign({ _id: account.user }, process.env.JWT_SECRET, { expiresIn: '1d', issuer: 'localhost' });
        await createComment({
          content: chance.paragraph(),
          parent: comment._id,
          parentType: 'Comment',
        });
        console.log(`${account.firstname} ${account.lastname} responded to ${comment._id}`);
      }
    }
  }
};
const main = async () => {
  const start = Date.now();

  ApiService.init();
  await genrateBaseData();
  await genrateLikes();
  await generateComments();
  const end = Date.now();

  console.log(`Seeded in ${(end - start) / 1000} secondes`);
};
main();
