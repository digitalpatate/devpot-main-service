process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

const app = require('../../app');
const DevlogModel = require('../../models/devlogModel');
const ProjectModel = require('../../models/projectModel');
const AccountModel = require('../../models/accountModel');
const jwtService = require('../../helpers/jwtService');


chai.use(chaiHttp);
const { expect, should, assert } = chai;

const { projectTemplate, devlogTemplate, accountTemplate } = require('../ObjectTemplates');


describe('DELETE /devlogs/:id', () => {
  let token;
  beforeEach((done) => {
    const newAccount = AccountModel(accountTemplate);
    newAccount.save((err) => {
      if (err) console.log(err);

      accountTemplate.id = newAccount._id;
      token = jwtService.createValidToken({ _id: accountTemplate.user });

      projectTemplate.owner = newAccount._id;

      const newProject = ProjectModel(projectTemplate);
      newProject.save((errProject) => {
        if (err) console.log(errProject);
        projectTemplate.id = newProject._id;
        devlogTemplate.project = newProject._id;

        const newDevlog = DevlogModel(devlogTemplate);
        newDevlog.save((errDevlog) => {
          if (err) console.log(errDevlog);
          devlogTemplate.id = newDevlog._id;
          done();
        });
      });
    });
  });
  it('try delete without auth token', (done) => {
    chai.request(app)
      .delete(`/devlogs/${devlogTemplate.id}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(401);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try delete with bad signature token', (done) => {
    chai.request(app)
      .delete(`/devlogs/${devlogTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createBadSignatureToken({ _id: accountTemplate.user })}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try delete with an expired token', (done) => {
    chai.request(app)
      .delete(`/devlogs/${devlogTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createExpiredToken({ _id: accountTemplate.user })}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try delete with wrong ownership', (done) => {
    chai.request(app)
      .delete(`/devlogs/${devlogTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createValidToken({ _id: mongoose.Types.ObjectId() })}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(403);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try delete an non-existent devlog', (done) => {
    chai.request(app)
      .delete(`/devlogs/${mongoose.Types.ObjectId()}`)
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(404);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Should delete the devlog', (done) => {
    chai.request(app)
      .delete(`/devlogs/${devlogTemplate.id}`)
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(false);
        DevlogModel.findById(devlogTemplate.id, (findErr, devlog) => {
          assert.equal(devlog, null);
          done();
        });
      });
  });
  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
