process.env.NODE_ENV = 'test';
const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

const DevlogModel = require('../../models/devlogModel');

const app = require('../../app');

chai.use(chaiHttp);
const { expect } = chai;
const { devlogTemplate } = require('../ObjectTemplates');

describe('GET /devlog:/:id', () => {
  let devlogId;
  before((done) => {
    const devlog = new DevlogModel(devlogTemplate);
    devlogId = devlog._id;

    devlog.save((err) => {
      if (err) console.log(err);
      done();
    });
  });
  it('Should return a devlog without error', (done) => {
    chai.request(app)
      .get(`/devlogs/${devlogId}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('data')
          .to.be.an('object').that.is.not.empty;
        expect(res.body)
          .have
          .property('error')
          .eq(false);
        done();
      });
  });
  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
