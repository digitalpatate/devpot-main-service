process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const JwtService = require('../../helpers/jwtService');

const app = require('../../app');

const ProjectModel = require('../../models/projectModel');
const AccountModel = require('../../models/accountModel');
const DevlogModel = require('../../models/accountModel');


chai.use(chaiHttp);
const { expect } = chai;

const { projectTemplate, devlogTemplate, accountTemplate } = require('../ObjectTemplates');

describe('POST /devlogs', () => {
  before((done) => {
    const newAccount = AccountModel(accountTemplate);
    newAccount.save((err) => {
      if (err) console.log(err);
      accountTemplate.id = newAccount._id;
      projectTemplate.owner = newAccount._id;
      const newProject = ProjectModel(projectTemplate);
      newProject.save((errProject) => {
        if (err) console.log(errProject);
        projectTemplate.id = newProject._id;
        devlogTemplate.project = newProject._id;
        done();
      });
    });
  });
  it('Create a devlog without title', (done) => {
    chai.request(app)
      .post('/devlogs')
      .set('Authorization', `Bearer ${JwtService.createValidToken({ _id: accountTemplate.user })}`)
      .send({ content: devlogTemplate.content, project: projectTemplate.id })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Create a devlog without content', (done) => {
    chai.request(app)
      .post('/devlogs')
      .set('Authorization', `Bearer ${JwtService.createValidToken({ _id: accountTemplate.user })}`)
      .send({ title: devlogTemplate.title, project: projectTemplate.id })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Create a devlog without project id', (done) => {
    chai.request(app)
      .post('/devlogs')
      .set('Authorization', `Bearer ${JwtService.createValidToken({ _id: accountTemplate.user })}`)
      .send({
        title: devlogTemplate.title, content: devlogTemplate.content,
      })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Create a devlog with a fake project id', (done) => {
    chai.request(app)
      .post('/devlogs')
      .set('Authorization', `Bearer ${JwtService.createValidToken({ _id: accountTemplate.user })}`)
      .send(devlogTemplate)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('data');
        expect(res.body)
          .have
          .property('error')
          .eq(false);
        done();
      });
  });
  afterEach((done) => {
    DevlogModel.deleteMany({});
    done();
  });
  after((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
