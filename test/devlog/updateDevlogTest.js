process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const app = require('../../app');
const DevlogModel = require('../../models/devlogModel');
const ProjectModel = require('../../models/projectModel');
const AccountModel = require('../../models/accountModel');
const jwtService = require('../../helpers/jwtService');



chai.use(chaiHttp);
const { expect, should, assert } = chai;

const { projectTemplate, devlogTemplate, accountTemplate } = require('../ObjectTemplates');


describe('PUT /devlogs/:id', () => {
  let token;
  beforeEach((done) => {
    const newAccount = AccountModel(accountTemplate);
    newAccount.save((err) => {
      if (err) console.log(err);

      accountTemplate.id = newAccount._id;
      token = jwt.sign({
        _id: newAccount.user,
        username: 'test',
      }, process.env.JWT_SECRET, { expiresIn: '1d' });

      projectTemplate.owner = newAccount._id;

      const newProject = ProjectModel(projectTemplate);
      newProject.save((errProject) => {
        if (err) console.log(errProject);
        projectTemplate.id = newProject._id;
        devlogTemplate.project = newProject.id;

        const newDevlog = DevlogModel(devlogTemplate);
        newDevlog.save((errDevlog) => {
          if (err) console.log(errDevlog);
          devlogTemplate.id = newDevlog._id;
          done();
        });
      });
    });
  });
  it('try update without auth token', (done) => {
    const newTitle = 'Updated title';
    chai.request(app)
      .put(`/devlogs/${devlogTemplate.id}`)
      .send({ title: newTitle })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(401);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try update with bad signature token', (done) => {
    const newTitle = 'Updated title';
    chai.request(app)
      .put(`/devlogs/${devlogTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createBadSignatureToken({ _id: accountTemplate.user })}`)
      .send({ title: newTitle })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try update with an expired token', (done) => {
    const newTitle = 'Updated title';
    chai.request(app)
      .put(`/devlogs/${devlogTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createExpiredToken({ _id: accountTemplate.user })}`)
      .send({ title: newTitle })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try update with wrong ownership', (done) => {
    const newTitle = 'Updated title';
    chai.request(app)
      .put(`/devlogs/${devlogTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createValidToken({ _id: mongoose.Types.ObjectId() })}`)
      .send({ title: newTitle })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(403);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try update an non-existent devlog', (done) => {
    const newTitle = 'Updated title';
    chai.request(app)
      .put(`/devlogs/${mongoose.Types.ObjectId()}`)
      .set('Authorization', `Bearer ${token}`)
      .send({ title: newTitle })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(404);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });

  describe('Test with mocked project', () => {
    it('try update the id', (done) => {
      chai.request(app)
        .put(`/devlogs/${devlogTemplate.id}`)
        .set('Authorization', `Bearer ${token}`)
        .send({ _id: mongoose.Types.ObjectId() })
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(400);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('error')
            .eq(true);
          done();
        });
    });
    it('try update the project id', (done) => {
      chai.request(app)
        .put(`/devlogs/${devlogTemplate.id}`)
        .set('Authorization', `Bearer ${token}`)
        .send({ project: mongoose.Types.ObjectId() })
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(400);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('error')
            .eq(true);
          done();
        });
    });
    it('try update the content', (done) => {
      chai.request(app)
        .put(`/devlogs/${devlogTemplate.id}`)
        .set('Authorization', `Bearer ${token}`)
        .send({ content: 'New content' })
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(200);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('error')
            .eq(false);
          expect(res.body)
            .have
            .property('error')
            .eq(false);

          DevlogModel.findById(devlogTemplate.id, (findErr, devlog) => {
            if (findErr) console.log(findErr);
            assert.equal(devlog.content, 'New content');
            done();
          });
        });
    });
    it('try update the content and title', (done) => {
      chai.request(app)
        .put(`/devlogs/${devlogTemplate.id}`)
        .set('Authorization', `Bearer ${token}`)
        .send({ title: 'New title', content: 'New content' })
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(200);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('error')
            .eq(false);
          expect(res.body)
            .have
            .property('error')
            .eq(false);

          DevlogModel.findById(devlogTemplate.id, (findErr, devlog) => {
            if (findErr) console.log(findErr);
            assert.equal(devlog.content, 'New content');
            assert.equal(devlog.title, 'New title');
            done();
          });
        });
    });
  });
  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
