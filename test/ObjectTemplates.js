const mongoose = require('mongoose');
const fs = require('fs-extra');

const accountTemplate = {
  user: mongoose.Types.ObjectId(),
  firstname: 'firstname',
  lastname: 'lastname',
  biography: 'lorem ipsum dolo',
};
const devlogTemplate = {
  title: 'Ceci est un test',
  content: 'Un super long content',
  project: mongoose.Types.ObjectId(),
};
const projectTemplate = {
  title: 'Ceci est un test',
  description: 'Une super long description',
  owner: mongoose.Types.ObjectId(),
};
const commentTemplate = {
  content: 'Un super commentaire de la mort qui tue',
  parent: mongoose.Types.ObjectId(),
  parentType: 'Devlog',
  author: mongoose.Types.ObjectId(),
};
module.exports = {
  accountTemplate,
  devlogTemplate,
  projectTemplate,
  commentTemplate,
};
