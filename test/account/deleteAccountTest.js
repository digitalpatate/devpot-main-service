process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const jwtService = require('../../helpers/jwtService');

const app = require('../../app');
const AccountModel = require('../../models/accountModel');


chai.use(chaiHttp);
const { expect, should, assert } = chai;

const { accountTemplate } = require('../ObjectTemplates');

describe('DELETE /accounts/:id', () => {
  let token;
  beforeEach((done) => {
    const newAccount = AccountModel(accountTemplate);
    newAccount.save((err) => {
      if (err) console.log(err);
      accountTemplate.id = newAccount._id;
      token = jwtService.createValidToken({ _id: newAccount.user });
      done();
    });
  });
  it('try delete without auth token', (done) => {
    chai.request(app)
      .delete(`/accounts/${accountTemplate.id}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(401);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try delete with bad signature token', (done) => {
    chai.request(app)
      .delete(`/accounts/${accountTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createBadSignatureToken({ _id: accountTemplate.user })}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try delete with an expired token', (done) => {
    chai.request(app)
      .delete(`/accounts/${accountTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createExpiredToken({ _id: accountTemplate.user })}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try delete with wrong ownership', (done) => {
    chai.request(app)
      .delete(`/accounts/${accountTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createValidToken({ _id: mongoose.Types.ObjectId() })}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(403);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try delete an non-existent account', (done) => {
    chai.request(app)
      .delete(`/accounts/${mongoose.Types.ObjectId()}`)
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(404);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Should delete the account', (done) => {
    chai.request(app)
      .delete(`/accounts/${accountTemplate.id}`)
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(false);
        AccountModel.findById(accountTemplate.id, (findErr, account) => {
          assert.equal(account, null);
          done();
        });
      });
  });
  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
