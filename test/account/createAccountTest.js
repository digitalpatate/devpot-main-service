process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const jwtService = require('../../helpers/jwtService');


const app = require('../../app');

chai.use(chaiHttp);
const { expect } = chai;
const { accountTemplate } = require('../ObjectTemplates');


describe('POST /accounts', () => {
  it('Create an account without user reference', (done) => {
    chai.request(app)
      .post('/accounts')
      .set('Authorization', `Bearer ${jwtService.createValidToken({ _id: accountTemplate.user })}`)
      .send({ lastname: accountTemplate.lastname, firstname: accountTemplate.firstname })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Create an account with an invalid user reference', (done) => {
    chai.request(app)
      .post('/accounts')
      .set('Authorization', `Bearer ${jwtService.createValidToken({ _id: accountTemplate.user })}`)
      .send({ lastname: accountTemplate.lastname, firstname: accountTemplate.firstname, user: 'inValidId' })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Create an account without the non-required field', (done) => {
    chai.request(app)
      .post('/accounts')
      .set('Authorization', `Bearer ${jwtService.createValidToken({ _id: accountTemplate.user })}`)
      .send({ user: accountTemplate.user })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(false);
        done();
      });
  });
  it('Create an account with the non-required field', (done) => {
    chai.request(app)
      .post('/accounts')
      .set('Authorization', `Bearer ${jwtService.createValidToken({ _id: accountTemplate.user })}`)
      .send(accountTemplate)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(false);
        done();
      });
  });

  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
