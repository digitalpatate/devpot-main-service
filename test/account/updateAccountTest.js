process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const app = require('../../app');
const AccountModel = require('../../models/accountModel');

chai.use(chaiHttp);
const { expect, should, assert } = chai;

const { accountTemplate } = require('../ObjectTemplates');


describe('PUT /accounts/:id', () => {
  let token;
  beforeEach((done) => {
    const newAccount = AccountModel(accountTemplate);
    newAccount.save((err) => {
      if (err) console.log(err);
      accountTemplate.id = newAccount._id;
      token = jwt.sign({ _id: newAccount.user, username: 'test' }, process.env.JWT_SECRET, { expiresIn: '1d' });
      done();
    });
  });
  it('try update without auth token', (done) => {
    const newFirstname = 'Updated firtname';
    chai.request(app)
      .put(`/accounts/${accountTemplate.id}`)
      .send({ firstname: newFirstname })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(401);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try update with bad signature token', (done) => {
    const badToken = jwt.sign({ _id: mongoose.Types.ObjectId(), username: 'test' }, 'wrongSecret', { expiresIn: '1d' });
    const newFirstname = 'Updated firstname';
    chai.request(app)
      .put(`/accounts/${accountTemplate.id}`)
      .set('Authorization', `Bearer ${badToken}`)
      .send({ firstname: newFirstname })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try update with an expired token', (done) => {
    const badToken = jwt.sign({ _id: mongoose.Types.ObjectId(), username: 'test' }, process.env.JWT_SECRET, { expiresIn: Math.floor(Date.now() / 1000) - (60 * 60) });
    const newFirstname = 'Updated firstname';
    chai.request(app)
      .put(`/accounts/${accountTemplate.id}`)
      .set('Authorization', `Bearer ${badToken}`)
      .send({ firstname: newFirstname })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(403);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try update with wrong ownership', (done) => {
    const badToken = jwt.sign({ _id: mongoose.Types.ObjectId(), username: 'test' }, process.env.JWT_SECRET, { expiresIn: Math.floor(Date.now() / 1000) - (60 * 60) });
    const newFirstname = 'Updated firstname';
    chai.request(app)
      .put(`/accounts/${accountTemplate.id}`)
      .set('Authorization', `Bearer ${badToken}`)
      .send({ title: newFirstname })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(403);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try update an non-existent account', (done) => {
    const newFirstname = 'Updated firstname';
    chai.request(app)
      .put(`/accounts/${mongoose.Types.ObjectId()}`)
      .set('Authorization', `Bearer ${token}`)
      .send({ firstname: newFirstname })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(404);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });

  it('try update the user reference', (done) => {
    chai.request(app)
      .put(`/accounts/${accountTemplate.id}`)
      .set('Authorization', `Bearer ${token}`)
      .send({ user: mongoose.Types.ObjectId() })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });

  it('Update the firstname without errors', (done) => {
    chai.request(app)
      .put(`/accounts/${accountTemplate.id}`)
      .set('Authorization', `Bearer ${token}`)
      .send({ firstname: 'New firstname' })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(false);
        done();
      });
  });

  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
