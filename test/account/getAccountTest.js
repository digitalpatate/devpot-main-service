process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

const app = require('../../app');
const AccountModel = require('../../models/accountModel');


chai.use(chaiHttp);
const { expect, should, assert } = chai;

const { accountTemplate } = require('../ObjectTemplates');

describe('GET /accounts', () => {
  beforeEach((done) => {
    const newAccount = AccountModel(accountTemplate);
    newAccount.save((err) => {
      if (err) console.log(err);
      accountTemplate.id = newAccount._id;
      done();
    });
  });
  it('Get the account by userid ?user={...}', (done) => {
    chai.request(app)
      .get(`/accounts?user=${accountTemplate.user}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(false);
        done();
      });
  });
  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
