process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

const app = require('../../app');
const AccountModel = require('../../models/accountModel');
const ProjectModel = require('../../models/projectModel');

const jwtService = require('../../helpers/jwtService');


chai.use(chaiHttp);
const { expect, should, assert } = chai;

const { accountTemplate, projectTemplate } = require('../ObjectTemplates');


describe('POST /account/:id/favourites/:projectId', () => {
  beforeEach((done) => {
    const newAccount = AccountModel(accountTemplate);
    newAccount.save((accountErr) => {
      if (accountErr) console.log(accountErr);
      accountTemplate.id = newAccount._id;
      projectTemplate.owner = accountTemplate.id;
      const newProject = ProjectModel(projectTemplate);
      newProject.save((projectErr) => {
        if (projectErr) console.log(projectErr);
        projectTemplate.id = newProject._id;
        done();
      });
    });
  });

  // Token bla bla and authorisation

  // wrong account

  // wrong project

  // normal
  it('Should add a project to favorite', (done) => {
    chai.request(app)
      .post(`/accounts/${accountTemplate.id}/favourites/${projectTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createValidToken({ _id: accountTemplate.user })}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(false);
        done();
      });
  });

  // Anti normal
  describe('Mock a project with a project in favorite', () => {
    beforeEach((done) => {
      AccountModel.findOne(accountTemplate.id, (findErr, account) => {
        account.favourites.push(mongoose.Types.ObjectId(projectTemplate.id));
        ProjectModel.findOne(projectTemplate.id, (err, project) => {
          project.favouritesCount = 1;
          project.save((err1) => {
            if (err1) console.log(err1);
            account.save((err2) => {
              if (err1) console.log(err2);
              done();
            });
          });
        });
      });
    });
    it('Should remove a project from favorite', (done) => {
      chai.request(app)
        .post(`/accounts/${accountTemplate.id}/favourites/${projectTemplate.id}`)
        .set('Authorization', `Bearer ${jwtService.createValidToken({ _id: accountTemplate.user })}`)
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(200);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('error')
            .eq(false);
          AccountModel.findOne(accountTemplate.id, (findErr, accout) => {
            assert.lengthOf(accout.favourites, 0);
            ProjectModel.findOne(projectTemplate.id, (findErr2, project) => {
              assert.equal(project.favouritesCount, 0);
              done();
            });
          });
        });
    });
  });


  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
