process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const jwtService = require('../../helpers/jwtService');
const path = require('path');

const app = require('../../app');


chai.use(chaiHttp);
const { expect } = chai;
const { projectTemplate, accountTemplate } = require('../ObjectTemplates');
const AccountModel = require('../../models/accountModel');

describe('POST /projects', () => {
  let token;
  beforeEach((done) => {
    const newAccount = AccountModel(accountTemplate);
    newAccount.save((saveErr) => {
      if (saveErr) console.log(saveErr);
      projectTemplate.owner = newAccount._id;
      token = jwtService.createValidToken({ _id: accountTemplate.user });
      done();
    });
  });
  it('Create a project without title', (done) => {
    chai.request(app)
      .post('/projects')
      .set('Authorization', `Bearer ${token}`)
      .attach('file', path.resolve(__dirname, './test.jpg'))
      .field('description', projectTemplate.description)
      .type('form')
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Create a project without description', (done) => {
    chai.request(app)
      .post('/projects')
      .set('Authorization', `Bearer ${token}`)
      .attach('file', path.resolve(__dirname, './test.jpg'))
      .field('title', projectTemplate.title)
      .type('form')
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Create a project without errors', (done) => {
    chai.request(app)
      .post('/projects')
      .set('Authorization', `Bearer ${token}`)
      .attach('file', path.resolve(__dirname, './test.jpg'))
      .field('description', projectTemplate.description)
      .field('title', projectTemplate.title)
      .type('form')
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('data');
        expect(res.body)
          .have
          .property('error')
          .eq(false);
        done();
      });
  });
  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
