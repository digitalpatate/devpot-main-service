process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');

const app = require('../../app');
const DevlogModel = require('../../models/devlogModel');
const ProjectModel = require('../../models/projectModel');


chai.use(chaiHttp);
const { expect } = chai;
const { projectTemplate, devlogTemplate } = require('../ObjectTemplates');

let projectId = '';
describe('GET /projects', () => {
  before((done) => {
    for (let i = 0; i < 5; i++) {
      const project = new ProjectModel(projectTemplate);
      projectId = project._id;
      project.save((err) => {
        if (err) console.log(err);
      });
    }
    done();
  });

  it('Should get all projects', (done) => {
    chai.request(app)
      .get('/projects')
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('data')
          .to.be.an('array').that.is.not.empty;
        expect(res.body)
          .have
          .property('error')
          .eq(false);
        done();
      });
  });
});
// Get project's devlogs -> create multiple devlog with mocked project id

describe('GET /projects/:id/devlogs', () => {
  before((done) => {
    const project = new ProjectModel(projectTemplate);

    project.save((err) => {
      if (err) console.log(err);
      for (let i = 0; i < 50; i++) {
        devlogTemplate.project = project._id;
        projectId = project._id;
        const devlog = new DevlogModel(devlogTemplate);
        devlog.save((errd) => {
          if (errd) console.log(errd);
        });
      }
      done();
    });
  });
  it('Should return devlogs of a project', (done) => {
    chai.request(app)
      .get(`/projects/${projectId}/devlogs`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('data')
          .to.be.an('array').that.is.not.empty;
        expect(res.body)
          .have
          .property('error')
          .eq(false);
        done();
      });
  });
});
