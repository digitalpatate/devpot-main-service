process.env.NODE_ENV = 'test';

const chai = require('chai');
const path = require('path');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const fs = require('fs-extra');
const jwtService = require('../../helpers/jwtService');


const app = require('../../app');
const ProjectModel = require('../../models/projectModel');
const AccountModel = require('../../models/accountModel');


chai.use(chaiHttp);
const { expect, should, assert } = chai;

const { projectTemplate, accountTemplate } = require('../ObjectTemplates');


describe('PUT /projects/:id', () => {
  let token;
  beforeEach((done) => {
    const newAccount = AccountModel(accountTemplate);
    newAccount.save((saveErr) => {
      if (saveErr) console.log(saveErr);

      accountTemplate.id = newAccount._id;
      projectTemplate.owner = accountTemplate.id;

      token = jwtService.createValidToken({ _id: newAccount.user });

      projectTemplate.imagePath = 'test.jpg';
      const newProject = ProjectModel(projectTemplate);
      newProject.save((saveErr2) => {
        if (saveErr) console.log(saveErr2);
        fs.copyFileSync(path.resolve(__dirname, './test.jpg'), `${process.env.UPLOAD_PATH}/project/${newProject._id}/test.jpg`);
        projectTemplate.id = newProject.id;
        done();
      });
    });
  });
  it('try update without auth token', (done) => {
    const newTitle = 'Updated title';
    chai.request(app)
      .put(`/projects/${projectTemplate.id}`)
      .send({ title: newTitle })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(401);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try update with bad signature token', (done) => {
    const newTitle = 'Updated title';
    chai.request(app)
      .put(`/projects/${projectTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createBadSignatureToken({ _id: accountTemplate.id })}`)
      .send({ title: newTitle })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try update with an expired token', (done) => {
    const newTitle = 'Updated title';
    chai.request(app)
      .put(`/projects/${projectTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createExpiredToken({ _id: accountTemplate.id })}`)
      .send({ title: newTitle })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try update with wrong ownership', (done) => {
    const newTitle = 'Updated title';
    chai.request(app)
      .put(`/projects/${projectTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createValidToken({ _id: mongoose.Types.ObjectId() })}`)
      .send({ title: newTitle })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(403);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try update an non-existent project', (done) => {
    const newTitle = 'Updated title';
    chai.request(app)
      .put(`/projects/${mongoose.Types.ObjectId()}`)
      .set('Authorization', `Bearer ${token}`)
      .send({ title: newTitle })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(404);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });

  describe('Test with mocked project', () => {
    it('try update the id', (done) => {
      chai.request(app)
        .put(`/projects/${projectTemplate.id}`)
        .set('Authorization', `Bearer ${token}`)
        .send({ _id: mongoose.Types.ObjectId() })
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(400);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('error')
            .eq(true);
          done();
        });
    });
    it('try update the owner', (done) => {
      chai.request(app)
        .put(`/projects/${projectTemplate.id}`)
        .set('Authorization', `Bearer ${token}`)
        .send({ owner: mongoose.Types.ObjectId() })
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(400);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('error')
            .eq(true);
          done();
        });
    });
    it('try update the description', (done) => {
      chai.request(app)
        .put(`/projects/${projectTemplate.id}`)
        .set('Authorization', `Bearer ${token}`)
        .send({ description: 'New description' })
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(200);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('error')
            .eq(false);
          expect(res.body)
            .have
            .property('error')
            .eq(false);
          ProjectModel.findById(projectTemplate.id, (findErr, project) => {
            if (findErr) console.log(findErr);
            assert.equal(project.description, 'New description');
            done();
          });
        });
    });
    it('try update the image', (done) => {
      chai.request(app)
        .put(`/projects/${projectTemplate.id}`)
        .set('Authorization', `Bearer ${token}`)
        .attach('file', path.resolve(__dirname, './test.jpg'))
        .field('description', projectTemplate.description)
        .field('title', projectTemplate.title)
        .type('form')
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(200);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('error')
            .eq(false);
          done();
        });
    });

    it('try update the description and title', (done) => {
      chai.request(app)
        .put(`/projects/${projectTemplate.id}`)
        .set('Authorization', `Bearer ${token}`)
        .send({ title: 'New title', description: 'New description' })
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(200);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('error')
            .eq(false);
          expect(res.body)
            .have
            .property('error')
            .eq(false);

          ProjectModel.findById(projectTemplate.id, (findErr, project) => {
            if (findErr) console.log(findErr);
            assert.equal(project.description, 'New description');
            assert.equal(project.title, 'New title');
            done();
          });
        });
    });
  });
  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
