process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const app = require('../../app');
const ProjectModel = require('../../models/projectModel');
const AccountModel = require('../../models/accountModel');
const jwtService = require('../../helpers/jwtService');


chai.use(chaiHttp);
const { expect, should, assert } = chai;

const { projectTemplate, accountTemplate } = require('../ObjectTemplates');


describe('DELETE /projects/:id', () => {
  beforeEach((done) => {
    const newAccount = AccountModel(accountTemplate);
    newAccount.save((accountErr) => {
      if (accountErr) console.log(accountErr);
      accountTemplate.id = newAccount._id;
      projectTemplate.owner = accountTemplate.id;
      const newProject = ProjectModel(projectTemplate);
      newProject.save((projectErr) => {
        projectTemplate.id = newProject._id;
        done();
      });
    });
  });
  it('try delete without auth token', (done) => {
    chai.request(app)
      .delete(`/projects/${projectTemplate.id}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(401);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try delete with bad signature token', (done) => {
    chai.request(app)
      .delete(`/projects/${projectTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createBadSignatureToken({ _id: accountTemplate.user })}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try delete with an expired token', (done) => {
    chai.request(app)
      .delete(`/projects/${projectTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createExpiredToken({ _id: accountTemplate.user })}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try delete with wrong ownership', (done) => {
    chai.request(app)
      .delete(`/projects/${projectTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createValidToken({ _id: mongoose.Types.ObjectId() })}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(403);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try delete an non-existent project', (done) => {
    chai.request(app)
      .delete(`/projects/${mongoose.Types.ObjectId()}`)
      .set('Authorization', `Bearer ${jwtService.createValidToken({ _id: accountTemplate.user })}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(404);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Should delete the project', (done) => {
    chai.request(app)
      .delete(`/projects/${projectTemplate.id}`)
      .set('Authorization', `Bearer ${jwtService.createValidToken({ _id: accountTemplate.user })}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(false);
        done();
      });
  });

  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
