process.env.NODE_ENV = 'test';

const mongoose = require('mongoose');
const fs = require('fs-extra');
const memoryServer = require('mongodb-memory-server');

const MongoMemoryServer = new memoryServer.MongoMemoryServer();

before(async () => {
  const memoryServerInfo = await MongoMemoryServer.getConnectionString();
  await mongoose.connect(memoryServerInfo, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  }, (err) => {
    if (err) console.error(err);
  });
});

after(async () => {
  await mongoose.disconnect();
  await MongoMemoryServer.stop();

  fs.removeSync(process.env.UPLOAD_PATH);
});
