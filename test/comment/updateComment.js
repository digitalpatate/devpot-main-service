process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

const app = require('../../app');

const CommentModel = require('../../models/commentModel');
const AccountModel = require('../../models/accountModel');

const JwtService = require('../../helpers/jwtService');

chai.use(chaiHttp);
const { expect, assert } = chai;

const { accountTemplate, commentTemplate } = require('../ObjectTemplates');

describe('PUT /comments', () => {
  before((done) => {
    const newAccount = AccountModel(accountTemplate);
    newAccount.save((err) => {
      if (err) console.log(err);
      accountTemplate.id = newAccount._id;
      commentTemplate.author = newAccount._id;
      done();
    });
  });
  beforeEach((done) => {
    const newComment = CommentModel(commentTemplate);
    newComment.save((saveErr) => {
      if (saveErr) console.log(saveErr);
      commentTemplate.id = newComment._id;
      done();
    });
  });
  it('try to update without auth token', (done) => {
    chai.request(app)
      .put(`/comments/${commentTemplate.id}`)
      .send({ content: 'Modified content' })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(401);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try to update an non-existent comment', (done) => {
    chai.request(app)
      .put(`/comments/${mongoose.Types.ObjectId()}`)
      .set('Authorization', `Bearer ${JwtService.createValidToken({ _id: accountTemplate.user })}`)
      .send({ content: 'Modified content' })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(404);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('try to change the parent reference', (done) => {
    chai.request(app)
      .put(`/comments/${commentTemplate.id}`)
      .set('Authorization', `Bearer ${JwtService.createValidToken({ _id: accountTemplate.user })}`)
      .send({ parent: mongoose.Types.ObjectId() })
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Try to change the type of the parent', (done) => {
    chai.request(app)
      .put(`/comments/${commentTemplate.id}`)
      .send({ parentType: 'Project' })
      .set('Authorization', `Bearer ${JwtService.createValidToken({ _id: accountTemplate.user })}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(400);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it(' try to update a wrong ownership comment', (done) => {
    chai.request(app)
      .put(`/comments/${commentTemplate.id}`)
      .send(accountTemplate)
      .set('Authorization', `Bearer ${JwtService.createValidToken({ _id: mongoose.Types.ObjectId() })}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(403);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(true);
        done();
      });
  });
  it('Should update a comment without error', (done) => {
    chai.request(app)
      .put(`/comments/${commentTemplate.id}`)
      .send(accountTemplate)
      .set('Authorization', `Bearer ${JwtService.createValidToken({ _id: accountTemplate.user })}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(false);
        done();
      });
  });
  afterEach((done) => {
    CommentModel.deleteMany({});
    done();
  });
  after((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
