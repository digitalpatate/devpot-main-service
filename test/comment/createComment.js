process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

const app = require('../../app');

const CommentModel = require('../../models/commentModel');
const ProjectModel = require('../../models/projectModel');
const AccountModel = require('../../models/accountModel');

const JwtService = require('../../helpers/jwtService');

chai.use(chaiHttp);
const { expect } = chai;

const { projectTemplate, accountTemplate, commentTemplate } = require('../ObjectTemplates');

describe('POST /comments', () => {
  beforeEach((done) => {
    const newAccount = AccountModel(accountTemplate);
    newAccount.save((err) => {
      if (err) console.log(err);
      accountTemplate.id = newAccount._id;
      projectTemplate.owner = newAccount._id;
      done();
    });
  });

  describe('Create a comment to a project', () => {
    beforeEach((done) => {
      const newProject = ProjectModel(projectTemplate);
      newProject.save((errProject) => {
        if (errProject) console.log(errProject);
        projectTemplate.id = newProject._id;
        commentTemplate.parent = newProject._id;
        commentTemplate.parentType = 'Project';
        done();
      });
    });
    it('Try to create a comment to an non-existant project', (done) => {
      commentTemplate.parent = mongoose.Types.ObjectId();
      chai.request(app)
        .post('/comments')
        .set('Authorization', `Bearer ${JwtService.createValidToken({ _id: accountTemplate.user })}`)
        .send(commentTemplate)
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(404);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('error')
            .eq(true);
          done();
        });
    });
    it('Should create a comment without errors', (done) => {
      chai.request(app)
        .post('/comments')
        .set('Authorization', `Bearer ${JwtService.createValidToken({ _id: accountTemplate.user })}`)
        .send(commentTemplate)
        .end((err, res) => {
          expect(res.status)
            .to
            .be
            .eq(200);
          expect(res.body)
            .be
            .a('object');
          expect(res.body)
            .have
            .property('error')
            .eq(false);
          done();
        });
    });
    describe('Create a comment to an other comment', () => {
      beforeEach((done) => {
        const newComment = CommentModel(commentTemplate);
        newComment.save((saveErr) => {
          commentTemplate.id = newComment._id;
          commentTemplate.parentType = 'Comment';
          commentTemplate.parent = newComment._id;
          if (saveErr) console.log(saveErr);
          done();
        });
      });
      it('Should create a comment without errors', (done) => {
        chai.request(app)
          .post('/comments')
          .set('Authorization', `Bearer ${JwtService.createValidToken({ _id: accountTemplate.user })}`)
          .send(commentTemplate)
          .end((err, res) => {
            expect(res.status)
              .to
              .be
              .eq(200);
            expect(res.body)
              .be
              .a('object');
            expect(res.body)
              .have
              .property('error')
              .eq(false);
            done();
          });
      });
    });
  });
  afterEach((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
