process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

const app = require('../../app');

const CommentModel = require('../../models/commentModel');
const AccountModel = require('../../models/accountModel');

const JwtService = require('../../helpers/jwtService');

chai.use(chaiHttp);
const { expect, assert } = chai;

const { accountTemplate, commentTemplate } = require('../ObjectTemplates');

describe('DELETE /comments', () => {
  before((done) => {
    const newAccount = AccountModel(accountTemplate);
    newAccount.save((err) => {
      if (err) console.log(err);
      accountTemplate.id = newAccount._id;
      commentTemplate.author = accountTemplate.id;
      done();
    });
  });
  beforeEach((done) => {
    const newComment = CommentModel(commentTemplate);
    newComment.save((saveErr) => {
      if (saveErr) console.log(saveErr);
      commentTemplate.id = newComment._id;
      done();
    });
  });
  it('Should delete a comment without error', (done) => {
    chai.request(app)
      .delete(`/comments/${commentTemplate.id}`)
      .set('Authorization', `Bearer ${JwtService.createValidToken({ _id: accountTemplate.user })}`)
      .end((err, res) => {
        expect(res.status)
          .to
          .be
          .eq(200);
        expect(res.body)
          .be
          .a('object');
        expect(res.body)
          .have
          .property('error')
          .eq(false);
        CommentModel.findById(commentTemplate.id, (findErr, comment) => {
          assert.equal(comment, null);
          done();
        });
      });
  });
  afterEach((done) => {
    CommentModel.deleteMany({});
    done();
  });
  after((done) => {
    mongoose.connection.db.dropDatabase();
    done();
  });
});
